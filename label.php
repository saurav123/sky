<?php 
$url = "https://opensky-network.org/api/states/all";
$json = file_get_contents($url);
$json_decoded = json_decode($json);

?>    
<!DOCTYPE html>
<html>
  <head>
    <title>Custom Markers</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
</script>
<style type="text/css">
  #map_wrapper {
    height: 600px;
}

#map_canvas {
    width: 100%;
    height: 100%;
}
</style>
  </head>
  <body>
<div id="map_wrapper">
    <div id="map_canvas" class="mapping"></div>
</div>

<script type="text/javascript">
  
  jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
     <?php foreach ($json_decoded->states as $key => $value) { ?>
        ['<?php echo $value[1]; ?>',<?php echo $value[5]; ?>,<?php echo $value[6]; ?>],
        <?php }  ?>
    ];
                        
    // Info Window Content
    var infoWindowContent = [
         <?php foreach ($json_decoded->states as $key => $value) { ?>
        ['<div class="info_content">' + '<h3><?php echo $value[2]; ?></h3>' + '<p>flight Number - <?php echo $value[1]; ?></p>' + '<p>altitude - <?php echo $value[7]; ?></p>' + '<a href="process.php?code=<?php echo $value[1]; ?>">view</a>' + '</div>'],
        <?php }  ?>
        
    ];

     var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icon = {
        url: "plane3.png", // url
        scaledSize: new google.maps.Size(30, 30), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: icon,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(8);
        google.maps.event.removeListener(boundsListener);
    });
    
}
</script>


  </body>
</html>


